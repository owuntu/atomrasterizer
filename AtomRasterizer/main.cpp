#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <limits>
#include <vector>

using namespace std;

void SavePPM(const uint8_t* pData, const std::size_t width, const std::size_t height, const std::string fileName)
{
    using namespace std;

    if (nullptr == pData)
    {
        std::cerr << "No image data to save!!.\n";
        return;
    }

    ofstream imgFile(fileName, ios::out | ios::binary);

    if (imgFile.is_open())
    {
        string tmpStr = "P6\n" + std::to_string(width) + " " + std::to_string(height) + "\n" + "255" + "\n";
        imgFile.write(tmpStr.c_str(), tmpStr.size() * sizeof(char));

        imgFile.write(reinterpret_cast<const char*>(pData), width * height * 3);

        imgFile.close();
    }
}

class Vec3
{
public:
    float x, y, z;

    Vec3() : x(0), y(0), z(0) {}
    Vec3(float ix, float iy, float iz) : x(ix), y(iy), z(iz) {}
    Vec3(const Vec3& rhs) : x(rhs.x), y(rhs.y), z(rhs.z) {}
    Vec3(float v) : x(v), y(v), z(v) {}

    const Vec3 operator+(const Vec3& rhs) const
    {
        Vec3 t;
        t.x = x + rhs.x;
        t.y = y + rhs.y;
        t.z = z + rhs.z;
        return t;
    }

    const Vec3 operator*(const float v) const
    {
        Vec3 t;
        t.x = x * v;
        t.y = y * v;
        t.z = z * v;
        return t;
    }

    const Vec3 Mul(const Vec3& rhs) const
    {
        Vec3 t;
        t.x = x * rhs.x;
        t.y = y * rhs.y;
        t.z = z * rhs.z;
        return t;
    }

    float Dot(const Vec3& rhs) const
    {
        return (x*rhs.x + y *rhs.y + z*rhs.z);
    }

    float LengthSq() const
    {
        return (x*x + y*y + z*z);
    }

    float Length() const 
    {
        return sqrt(this->LengthSq());
    }
};

class Triangle
{
public:
    Vec3 a, b, c; // vertex
    Vec3 color;

    Triangle() : a(0), b(0), c(0), color(1.0f) {}
    Triangle(const Vec3& ia, const Vec3& ib, const Vec3& ic) : a(ia), b(ib), c(ic), color(1.0f) {}
    Triangle(const Vec3& ia, const Vec3& ib, const Vec3& ic, const Vec3& icolor) : a(ia), b(ib), c(ic), color(icolor) {}
};

bool In01(float v)
{
    if (v <= 1.0f && v >= 0.0f)
    {
        return true;
    }
    return false;
}

float Clamp01(float v)
{
    if (v > 1.0f) return 1.0f;
    if (v < 0.0f) return 0.0f;
    return v;
}

inline uint8_t FloatToUint(float v)
{
    return static_cast<uint8_t>(255.f * v);
}

Vec3 ComputeBaricentric(float px, float py, const Triangle& tri)
{
    const Vec3& A = tri.a;
    const Vec3& B = tri.b;
    const Vec3& C = tri.c;

    float denmInv = (B.y - C.y) * (A.x - C.x) + (C.x - B.x) * (A.y - C.y);
    denmInv = 1.0f / denmInv;
    Vec3 lamb;
    lamb.x = ((B.y - C.y) * (px - C.x) + (C.x - B.x) * (py - C.y)) * denmInv;
    lamb.y = ((C.y - A.y) * (px - C.x) + (A.x - C.x) * (py - C.y)) * denmInv;
    lamb.z = 1.0f - lamb.x - lamb.y;
    return lamb;
}

static const int gs_MSAA = 4;

void main()
{
    float mbias = 1e-4f;
    int width = 512;
    int height = 512;
    int numPixels = width * height;
    vector<vector<Vec3> > img(numPixels, vector<Vec3>(gs_MSAA));
    vector<vector<float> > zBuffer(numPixels, vector<float>(gs_MSAA, std::numeric_limits<float>::max()));

    Triangle tris[] = {
        Triangle(Vec3(-2.f, -1.f, 2.f),
                 Vec3(2.f, -1.f, 1.f),
                 Vec3(1.f, 3.f, 2.f),
                 Vec3(.75f, .25f, .25f)),
        Triangle(Vec3(-2.f, -1.f, 1.f),
                 Vec3(2.f, 0.5f, 2.f),
                 Vec3(-1.f, 3.f, 1.f),
                 Vec3(.25f, .15f, .85f))
    };

    int numTri = sizeof(tris) / sizeof(Triangle);

    // For now, just assume the image plane is at (0, 0, 0) with normal (0,0,1), and the bound is (-5, -5) to (5, 5).
    // For simplicity, just using orthographic projection for now.
    float imgMaxX = 5.f;
    float imgMaxY = 5.f;
    float imgMinX = -5.f;
    float imgMinY = -5.f;

    float imgPWidth = imgMaxX - imgMinX;
    float imgPHeight = imgMaxY - imgMinY;

    float pxW = imgPWidth / static_cast<float>(width);
    float pxH = imgPHeight / static_cast<float>(height);

    // for each primitive
    for (int i = 0; i < numTri; ++i)
    {
        const Triangle& tri = tris[i];

        const Vec3& A = tri.a;
        const Vec3& B = tri.b;
        const Vec3& C = tri.c;

        float denmInv = (B.y - C.y) * (A.x - C.x) + (C.x - B.x) * (A.y - C.y);
        denmInv = 1.0f / denmInv;
        // for each pixel
        for (int pi = 0; pi < numPixels; ++pi)
        {
            // does the primitive cover the pixel?
            float pcx = static_cast<float>(pi % width) / static_cast<float>(width - 1) * imgPWidth + imgMinX;
            float pcy = static_cast<float>(pi / width) / static_cast<float>(height - 1) * imgPHeight + imgMinY;

            Vec3 fragColor = tri.color;
            for (int mi = 0; mi < gs_MSAA; ++mi)
            {
                float px = pcx + 0.25f * pxW * ((mi % 2) == 0 ? -1.0f : 1.f);
                float py = pcy + 0.25f * pxH * ((mi / 2) == 0 ? -1.0f : 1.f);
                float lamb1 = ((B.y - C.y) * (px - C.x) + (C.x - B.x) * (py - C.y)) * denmInv;
                float lamb2 = ((C.y - A.y) * (px - C.x) + (A.x - C.x) * (py - C.y)) * denmInv;
                float lamb3 = 1.0f - lamb1 - lamb2;

                if (lamb1 < mbias || lamb2 < mbias || lamb3 < mbias)
                {
                    // ignore
                    continue;
                }
                float ptz = lamb1 * A.z + lamb2 * B.z + lamb3 * C.z;

                if (ptz < zBuffer[pi][mi])
                {
                    // lit
                    img[pi][mi] = fragColor;
                    zBuffer[pi][mi] = ptz;
                }
            }
            
        }
    }

    uint8_t * pData = new uint8_t[numPixels * 3];

    for (int i = 0; i < numPixels; ++i)
    {
        Vec3 pixColor = Vec3(0.f);
        for (int mi = 0; mi < gs_MSAA; ++mi)
        {
            pixColor = pixColor + img[i][mi];
        }
        pixColor = pixColor * (1.f / static_cast<float>(gs_MSAA));
        pData[i * 3]     = FloatToUint(Clamp01(pixColor.x));
        pData[i * 3 + 1] = FloatToUint(Clamp01(pixColor.y));
        pData[i * 3 + 2] = FloatToUint(Clamp01(pixColor.z));
    }
    
    SavePPM(pData, width, height, "testImg.ppm");

    delete[] pData;
}